BITS 64

%DEFINE fgOS_VER '1'	; 
%DEFINE fgOS_API_VER 1  ;

disk_buffer	equ	24576

os_call_vectors:
    jmp os_main
    jmp 

os_main:
    cli
    mov rax,0
    mov ss,rax
    mov rsp,0FFFFh
    sti
    cld
    mov rax, 2000h
    mov ds,rax
    mov es,rax
    mov fs,rax
    mov gs,rax
    cmp rdi,0
    je no_change
    mov [bootdev], rdi
    push es
    mov rax,8